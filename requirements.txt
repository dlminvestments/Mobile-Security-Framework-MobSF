Django==3.2
pdfkit==0.6.1
androguard==3.3.5
lxml==4.6.3
rsa==4.7.2
biplist==1.0.3
requests==2.25.1
bs4==0.0.1
colorlog==5.0.1
macholib==1.14
google-play-scraper==0.2
whitenoise==5.2.0
waitress==2.0.0
gunicorn==20.1.0
psutil==5.8.0
shelljob==0.6.3
asn1crypto==1.4.0
oscrypto==1.2.1
distro==1.5.0
IP2Location==8.5.1
lief==0.11.4
http-tools==2.1.0
libsast==1.4.1
# Do not update the following
apkid==2.1.2
cryptography>=2.9,<4.0 # pyup: ignore
pyOpenSSL>=19.1.0,<20.1 # pyup: ignore
